---
title: "Initilisation questionnaire lime survey enquête réseau de chaleur"
author: "DREAL PdL"
date: "31/10/2019"
output: 
  html_document:
    toc: TRUE
    toc_depth: 4
    toc_float: TRUE
    css: uikit_Lato_Roboto_av_drealdown.css
    keep_md: TRUE
---





Le présent projet concerne un questionnaire d'enquête visant à recenser les réseaux de chaleur des Pays de la Loire. L'enquête s'appuie sur l'outil d'enquête en ligne Limesurvey, qui est alimenté par deux tables principales :  

* la *base de sondage* : comprennant la liste des organismes interrogés associés à quelques informations dont nous disposons au lancement de l'enquête (nombre et noms des réseaux de chaleur pré-identifiés)  
* la *table des réponses pré-remplies* comprennant toutes les informations sur les réseaux de chaleur, que les personnes interrogées vont modifier et enrichir lors de l'enquête au travers de limsesurvey.  

Les deux tables sont liées par un identifiant de questionnaire (token).  
L'objectif de cette fiche de procédure est de créer ces deux tables et de fournir les informations techniques utiles à l'interfaçage avec l'enquêteur en ligne Limesurvey pour initier l'enquête.  

## Adapter le questionnaire limesurvey

### Administration du questionnaire  
Le formulaire d'enquête est fourni au format ODT par la MECC.   
Il est stocké à l'adresse `T:/datalab/MECC_RESEAUX_CHALEUR/DONNEES_CLIENT/formulaire_enquete.odt`.     
Le présent repertoire de travail est situé sur `T:/datalab/MECC_RESEAUX_CHALEUR/PRODUCTION/test_lime_survey`.  

Le questionnaire est initié par la DOES sur LimeSurvey sous l'url d'aministration :
http://enqueteur-inter-maj.dreal-pays-de-la-loire.e2.rie.gouv.fr/index.php/admin/survey/sa/view/surveyid/799383 

Les droits ont été partagés avec   
admin 	SCTE/DSIT 	dreal44   
albin.perronnie 	– 	Albin PERRONIE (MECC)   
anita.robin-truffet 	– 	Anita Robin-Truffet (MECC)    

#### Activer le questionnaire
Pour que les fonctionnalités de pré-remplissage des réponses soient opérationnelles, le questionnaire est à activer avec les paramètres suivants : 
![](img/LS_activation_questionR.png)

Il faut ensuite charger les réponses et cliquer sur la fonction 'réitérer le questionnaire'.


### Augmentation du nombre de réseaux de chaleur à décrire  
La personne enquêtée peut répondre sur plusieurs réseaux de chaleur.  
Pour cela à la question n°3 elle renseigne le nb de réseaux de chaleurs qui la concerne.  

Il faut que le questionnaire prévoit suffisamment de fiches réseaux par répondant car on ne peut pas demander à limesurvey d'ajouter des groupes de questions tant que l'utilisateur répond oui à une question par exemple.  

Un réseau de chaleur est décrit à partir d'un groupe de questions. Pour le 1er réseau de chaleur, ce groupe a été exporté dans le répertoire du projet : `gp_questions_RC`, sous le noms : `Rc01.lsg`. Pour augmenter la capacité de réponse de chaque répondant (en nombre de réseaux), il convient de le dupliquer autant de fois que l'on souhaite proposer de fiches réseau.   


Avant réimport des fiches réseaux dupliquées dans limesurvey, il est nécessaire d'adapter le groupe de questions, au niveau :   

- des identifiants de questions,  
- du critère logique d'affichage du groupe, par exemple : _((Q00003.NAOK >= "1"))_ pour au moins 1 réseaux de chaleur renseigné à la question 3,  
- des critères logiques d'affichages des questions, par exemple : _((rc1id00.NAOK == "A1" or rc1id00.NAOK == "A4"))_ pour éviter d'afficher les questions relatives au réseau n°1 s'il le répondant ne connaît pas le réseau ou indique qu'il n'est plus en service
- le nom du réseau issu de la table des participants, qui figure dans l'intitulé du groupe par exemple _Réseau de chaleur n°1 {TOKEN:ATTRIBUTE_6}_  

Pour cela : ouvrir chacun des groupes de questions avec un éditeur de texte (notepad ou rstudio recommandés), l'enregistrer sous un autre nom (par exemple _Rc02.lsg_) et y rechercher/remplacer les éléments de texte suivants :   

| **txt recherché ds 1er RC exporté** | **txt pour RC n°2 à réimporter** | **txt pour le Xième RC à réimporter** | **nb occurrences** |  
| ----------------------------------- | -------------------------------- | ------------------------------------- | ------------- |   
| chaleur n°1 | chaleur n°2 | chaleur n°X | 1 |  
| CDATA[rc1 | CDATA[rc2 | CDATA[rcX | 31 |     
|  >= \"1\" |  >= \"2\" |  >= \"X\" | 1 |   
| {TOKEN:ATTRIBUTE_5} | {TOKEN:ATTRIBUTE_7} | {TOKEN:ATTRIBUTE_ __2X+3__} | 2 |   
| {TOKEN:ATTRIBUTE_6} | {TOKEN:ATTRIBUTE_8} | {TOKEN:ATTRIBUTE_ __2X+4__} | 3 |     


Nota : certains attributs de la table des participants progressent de 2 en 2 (en raison de l'id du réseau) alors que les autres s'incrémentent de 1 en 1.  

Pour réimporter un groupe de question dans limesurvey, aller sur _lister les groupes de questions / importer un groupe_.    
![](img/LS_import_gp.png)   
Laisser le paramètre _Convertir les liens de ressources ?_ à _Oui_.  
Après avoir cliquer sur _importer_, retourner en arrière (avec la flèche <-- de firefox) pour revenir sur l'interface d'importation d'un groupe et charger le groupe suivant.  

Rappel : il faut par ailleurs également adapter le nombre d'attributs de la table des participants pour que l'on puisse récupérer les noms du réseau de chaleur connus à décrire.  

Pour l'instant, pour les test, au maximum 20 réseaux de chaleurs peuvent être renseignés par les testeurs répondants.
En cas de modification de la structure du groupe de questions relatives à un fiche réseau de chaleur, la répercution des évolutions aux 19 autres groupes de réponses prend au moins 40 min, même avec bcp d'organisation.


### Invitations

Pour pouvoir suivre le taux de réponses, il est nécessaire d'activer la fonctionnalité 'invitations'. Tous les répondants doivent alors avoir un code token pour pouvoir répondre à l'enquête ou s'inscrire à partir de l'[url courte du questionnaire](http://enqueteur-inter-maj.dreal-pays-de-la-loire.e2.rie.gouv.fr/index.php/799383), avec leurs adresses e-mail, leur nom, leur organisme. Elles ne bénéficieront pas du pré-remplissage.

Pour charger une nouvelle table de répondants complète, il faut supprimer la base des participants. Il y a un bouton rouge pour cela, en haut à droite de l'interface "Participants au questionnaire" :  
![](img/LS_supp_ppants.png)

Puis ré-initialiser la base avant de pouvoir ajouter les contacts via le fichier csv. Utiliser les paramètres suivants :
![](img/LS_importer_ppants.png)

Après avoir chargé notre fichier, Limesurvey nous produit un rapport qui nous indique le nombre de participants importés.  

Rappel : lien vers la [fiche mode d'emploi du minitère sur les invitations](http://web.metier.e2.rie.gouv.fr/fichier/odt/Fiche-Invitations-v2_cle7cb7d8.odt?arg=262&cle=baa8b1b8b1c5433f42879d4ee3a4fa59ca855bbb&file=odt%2FFiche-Invitations-v2_cle7cb7d8.odt)

### Permettre plusieurs réponses avec la même URL est aujourd'hui impossible
Si cela semble nuire à l'objectif de collecter d'un maximum d'informations, cette situation facilite énormément la tâche d'élimination des doublons en phase d'exploitation des résultats et oblige les répondants d'une même organisation à se concerter pour ne fournir qu'une réponse cohérente.

#### Toute réponse validée, apportée sur une URL de questionnaire avec token, désactive ce token.  
<!-- Plusieurs recherches prometteuses mais non concluantes : cf [documentation]("archives/doc_LS_pb_doublons_token/") et [Paramétrer les URL](https://manual.limesurvey.org/URL_fields/fr)    -->
<!-- Il faudrait pouvoir ajouter ou modifier un script php à la racine de notre serveur internet limesurvey. -->

Des précautions sont prises pour en informer les personnes enquêtées :   

- il leur est signalé que leurs réponses pourront être modifiées/complétées par la personne avec qui ils partagent l'url seulement s'il choisisse de 'finir plus tard',  
- ils sont invités à envoyer leur réponses s'ils ne souhaitent pas que leurs réponses soient modifiées, ils recevront par ailleurs copie de leur réponse dans le mail de confirmation/notification détaillée.

En effet, en cas de participant qui aurait validé trop vite ses réponses et qui souhaiterait les modifier, on peut réactiver le token en allant [dans les participants](http://enqueteur-inter-maj.dreal-pays-de-la-loire.e2.rie.gouv.fr/index.php/admin/tokens/sa/browse/surveyid/799383#), en éditant les propriétés du participant concerné et en basculant le curseur "complété" à non.

![](img/LS_reactivation_token.png)

#### Créer un nouveau répondant

##### A - sans préremplissage de ses réponses  
simplement lui communiquer l'URL courte du questionnaire (sans code territoire ou token) : http://enqueteur-inter-maj.dreal-pays-de-la-loire.e2.rie.gouv.fr/index.php/799383

##### B- avec préremplissage des réponses  
Cette solution n'est à déployer qu'en cas d'insistance particulière du participants, elle conduira nécessairement à la création d'un doublon sur lequel il conviendra de revenir et réfléchir après coup.    

Pour chaque nouveau participant il faut ajouter une ligne dans la table des participants et une ligne dans la table des réponses.   

1. __ajouter un nouveau répondant__    
  * dans le fichier csv des répondants, copier-coller le profil territorial à dupliquer dans une nouvelle ligne  
  * modifier son token (par exemple 44001 devient 44001_1)  
  * modifier les données d'identité (firstname, lastname, email)  
  * enregistrer sous au format csv **en éditant les paramètres du filtre** :   
  ![](img/Enregis_csv_depuis_calc.png)   
  * réimporter la table des participants sans la supprimer au préalable, limesurvey n'ajoutera que les participants dont il ne connaît pas le token.  
  _cela peut également être réalisé directement dans l'interface de limesurvey dédiée aux participants, par le menu créer/ajouter des contacts, il faudra alors renseigner à la main les noms et identifiants des réseaux de chaleurs à associer au nouveau participant_

2. __lui associer ensuite les réponses à pré-saisies__ 
  * exporter les réponses format VV-csv (ou utiliser le fichier de départ format VV-csv)
  * ouvrir le fichier csv des réponses format VV avec notepad (calc chez moi ne gère pas le nombre de colonnes)  
  * dupliquer la précédente réponse du territoire et modifier les premiers champs pour lui associer le nouveau token (44001_1 dans notre exemple précédent) et les nouvelles données d'identité   
  * supprimer la première ligne d'entête de colonnes   
  * importer ces réponses au niveau de l'interface des réponses : http://enqueteur-inter-maj.dreal-pays-de-la-loire.e2.rie.gouv.fr/index.php/admin/dataentry/sa/vvimport/surveyid/799383  


## Préparer la base des personnes enquêtées (=table des participants ou base de sondage)

### Format d'entrée CSV pour limesurvey

Le fichier doit être un fichier CSV standard (délimiteur : virgule ou point virgule) avec éventuellement des guillemets pour encadrer les valeurs. La première ligne doit contenir le nom des champs. Les champs peuvent être donnés dans n'importe quel ordre.   
Champ(s) obligatoire(s) : firstname, lastname, email   
Champ(s) optionnel(s) : emailstatus, token, language, validfrom, validuntil, attribute_1, attribute_2, attribute_3, usesleft, ... ici :   

**token**	: siret epci d'appartenance de l'enquêté ou code insee de commune de l'enquêté (format texte), c'est la fin de l'url du questionnaire envoyé à chaque courriel, et c'est l'identifiant qui fait le lien entre participant et réponses préremplies  
**attribute_1**	: organisme d'appartenance  
**attribute_2**	: nb de réseaux dejà connus pour cet organisme  
**attribute_3**	: noms des réseaux dejà connus pour cet organisme  
**attribute_4** : type d'organisme (commune, epci) + siret epci si commune (pour faciliter la sélection)   
**attribute_5** : id du 1er réseau rattaché au mail  
**attribute_6** : nom du 1er réseau rattaché au mail  
**attribute_7** : id du 2e réseau rattaché au mail  
**attribute_8** : nom du 2e réseau rattaché au mail  
**attribute_9** : id du 3e réseau rattaché au mail  
**attribute_10** : nom du 3e réseau rattaché au mail  
**attribute_11** : id du 4e réseau rattaché au mail  
**attribute_12** : nom du 4e réseau rattaché au mail  
et plus, en fonction du nb de fiches réseau de chaleur retenu...   

<!-- **attribute_4** : longitude latitude de l'organisme d'appartenance (format 47.2383 -1.5603, récupéré sur https://www.openstreetmap.org/#map=13/47.4728/-0.5160)     -->

La signification de ces attributs est à definir à la main dans limesurvey, au niveau des participants ne l'enquête :  
`Enquête réseaux de chaleur / Participants au questionnaire / Gérer les champs d'attribut` 
Pour l'instant 40 attributs correspondant aux 20 fiches réseaux de chaleurs ont été déclarés. Attention à la place du 4e attribut (en toute fin).

Le fichier des participants est créé par les traitements ci-après, il est enregistré dans le répertoire des livrables du projet.

La table des attributs des participants est créée à partir d'un [export des participants](http://enqueteur-inter-maj.dreal-pays-de-la-loire.e2.rie.gouv.fr/index.php/admin/tokens/sa/exportdialog/surveyid/799383).





Lien vers la [fiche mode d'emploi du minitère sur les invitations](http://web.metier.e2.rie.gouv.fr/fichier/odt/Fiche-Invitations-v2_cle7cb7d8.odt?arg=262&cle=baa8b1b8b1c5433f42879d4ee3a4fa59ca855bbb&file=odt%2FFiche-Invitations-v2_cle7cb7d8.odt).

### Initialiser la base de sondage
A partir des [scripts de collecte d'annuaire.gouv.fr](https://gitlab.com/dreal-datalab/adresses-mairies-epci), on ébauche la base de sondage.   

L'annuaire de l'administration en ligne comporte encore quelques doublons suite à la réfonte de la carte de l'intercommunalité : 



```
## # A tibble: 4 x 3
##   token     attribute_1                              email                 
##   <chr>     <chr>                                    <chr>                 
## 1 200083392 Communauté d'agglomération - Laval       laval-agglo@agglo-lav~
## 2 200060010 Communauté d'agglomération - Mauges Com~ contact@maugescommuna~
## 3 200083392 Communauté de communes - Pays de Loiron  contact@cc-paysdeloir~
## 4 200060010 Communauté de communes de Saint Florent~ accueil@ccstflorent.fr
```

On améliore cette première base en substituant des contacts ciblés énergie-climat fournis par la MECC à certains contacts génériques, à partir du [fichier courriels_specifiques.ods](../../DONNEES_CLIENT/courriels_specifiques.ods) présent dans le répertoire DONNEES_CLIENT.




Ces derniers sont éliminés 'à la main'.  

## Préparer la base des réponses pour pré-saisies des réponses

### Passer d'un export RData à un export type VV qui peut être réimporté dans limesurvey
On peut maintenant exporter les réponses directement en .RData. Depuis l'[interface dédiée aux réponses]( http://enqueteur-inter-maj.dreal-pays-de-la-loire.e2.rie.gouv.fr/index.php/admin/export/sa/exportresults/surveyid/799383 ) : exporter la table des réponses dans le répertoire reponses aux format _R (fichier de syntaxe)_ puis _R (fichier de données)_ avec les paramètres d'exports suivants  :    

![](img/export_rdata.jpg)  

Pour le réimport des réponses, il faudra nécessairement utiliser les entêtes de l'export VV.   
Pour récupérer la structure du fichier, aller à la rubrique des réponses, utiliser l'export _Exporter vers un fichier de questionnaire VV_ et enregistrer le fichier csv dans le répertoire reponses.   
Les deux premières lignes correspondent aux libellés et aux codes des questions. On ne peut pas paramétrer cet export.  


```
## [1] TRUE
```

```
## [1] TRUE
```

```
## [1] TRUE
```
Les champs issus de la table des participants présents dans le RData ne sont logiquement pas dans le fichier de réponses VV.
Il faut en revanche réintroduire des champs inutiles comme : submitdate, lastpage, startlanguage, startdate, refurl, X

Le réimport des réponses se passe au niveau de l'[interface des réponses](http://enqueteur-inter-maj.dreal-pays-de-la-loire.e2.rie.gouv.fr/index.php/admin/dataentry/sa/vvimport/surveyid/799383), avec les paramètres suivants : 
![](img/LS_import_reponses_2.png)

Dans le fichiers des réponses exportées (VV), on dénombre 20 fiches réseau de chaleur.
Ce nombre correspond à ce qui est prévu et avait été annoncé plus haut.

### Croiser les sources existantes sur les réseaux de chaleurs et importer les données compilées comme réponses pré-remplies

Compiler les données fond chaleur Ademe, de la précédente enquête DREAL et les [données locales de l'énergie](https://www.statistiques.developpement-durable.gouv.fr/donnees-locales-de-consommation-denergie#chaleur-et-froid)
--> ce travail a été réalisé à la main par la MECC pour édition 2019 de l'enquête. ("DONNEES_CLIENT/2019-09-30_Base_RC_precompilee_MECCv2.ods")

Les liens entre les informations compilées fournies par la MECC et les champs d'enquêtes sont consignés dans un [tableur de référence intitulé ref_concordances_champs.xls](ref_concordances_champs.xls) et enregistré à la racine du projet (_T:/datalab/MECC_RESEAUX_CHALEUR/PRODUCTION/test_lime_survey_).

**Nota : Liste des champs issus de la compilation MECC inutilisés :**  

<!--html_preserve--><div id="htmlwidget-eec1b0953a00c9cf94c4" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-eec1b0953a00c9cf94c4">{"x":{"filter":"none","fillContainer":false,"autoHideNavigation":true,"data":[["nom","organisme","qualite","telephone","courriel","login_formulaire_en_ligne_login","mdp","Code EPCI","Libelle EPCI","Dpt","Date envoi courrier enquête","Date réception réponse","Observations","PUISSANCE","PCTCOG","PCTCOG_GAZ_NATUREL","PCTCOG_CHARBON","PCTCOG_FIOUL_DOMESTIQUE","PCTCOG_FIOUL_LOURD","PCTCOG_GPL","PCTCOG_BIOMASSE_SOLIDE","PCTCOG_DECHETS_INTERNES","PCTCOG_BIOGAZ","PCTCOG_AUTRES","CONSONA","CONSOA","Observations","Pas d’évol","Mix énergétique","Longueur","Chaleur livrée","Pas d’évol commentaire","Mix énergétique commentaire","Longueur commentaire","Chaleur livrée commentaire","Enquête 2015","SNCU","ADEME","ML","TEPCV","CANA","AM DPE","Atlanbois"],["issu de la table des participants (jointe sur code EPCI ou code commune)","issu de la table des participants (jointe sur code EPCI ou code commune)","issu de la table des participants (jointe sur code EPCI ou code commune)","issu de la table des participants (jointe sur code EPCI ou code commune)","issu de la table des participants (jointe sur code EPCI ou code commune)","code insee de la commune ou de l’epci","pas de mot de passe","calculé à partir du code commune ou des coordonnées OSM","calculé à partir du code commune ou des coordonnées OSM","calculé à partir du code commune ou des coordonnées OSM","généré automatiquement par lime survey","généré automatiquement par lime survey","un champ commentaire à ajouter ? a priori champ renseigné par celui qui saisissait les réponses","se dépuit par somme des autres champs puissance","pas dans questionnaire mecc","pas dans questionnaire mecc","pas dans questionnaire mecc","pas dans questionnaire mecc","pas dans questionnaire mecc","pas dans questionnaire mecc","pas dans questionnaire mecc","pas dans questionnaire mecc","pas dans questionnaire mecc","pas dans questionnaire mecc","inutile, conso d’un secteur inconnu","inutile, secteur agricole non identifié dans les types de bâti desservi","ne concernait que réseau privée, remplacé par vente de chaleur mis à 0","questions sur evolutions abandonnées","questions sur evolutions abandonnées","questions sur evolutions abandonnées","questions sur evolutions abandonnées","questions sur evolutions abandonnées","questions sur evolutions abandonnées","questions sur evolutions abandonnées","questions sur evolutions abandonnées","? - peut être plus tard","? - peut être plus tard","? - peut être plus tard","? - peut être plus tard","? - peut être plus tard","? - peut être plus tard","? - peut être plus tard","? - peut être plus tard"],["firstname, lastname","attribute1",null,null,"email","token",null,null,null,null,"submitdate","datestamp",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]],"container":"<table class=\"compact\">\n  <thead>\n    <tr>\n      <th>champ_mecc_supprimes<\/th>\n      <th>explication<\/th>\n      <th>reference<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"pageLength":43,"search":false,"order":[],"autoWidth":false,"orderClasses":false,"lengthMenu":[10,25,43,50,100]}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

**Nota : Liste des champs de l'enquête ajoutés ou calculés à partir des champs MECC :**  
<!--html_preserve--><div id="htmlwidget-ac1e6fa0920afc2c9086" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-ac1e6fa0920afc2c9086">{"x":{"filter":"none","fillContainer":false,"autoHideNavigation":true,"data":[["Code EPCI","Libelle EPCI","Dpt","firstname","lastname","attribute_1 (organisme)","attribute_2 (nb de réseaux par répondant)","email","token","submitdate","datestamp","id00 : statut de fonctionnement","id02 : implantation géographique","id10 : type de locaux desservis","id1 : chaleur vendue_SQ001_001","tec01 : type énergie","tecXX_SQ002 : Energie primaire consommée","tec07 : autres_enr","tec10 : autres_recup","tec13 : fioul","tec14 : autre fossile"],["à partir de la table des participants et du code commune compil RC MECC","à partir de la table des participants et du code commune compil RC MECC","à partir de la table des participants et du code commune compil RC MECC","à partir de la table des participants","à partir de la table des participants","à partir de la table des participants et du code commune compil RC MECC","à partir de la table des participants","à partir de la table des participants","à partir de la table des participants","à partir de la table des participants","à partir de la table des participants","en_projet_ou_en_service_statut et date_de_mise_en_service_du_reseau, en fonctionnement par défaut","geom sigloire ou centroïde depcom si absent sigloire","à partir CONSOR CONSOT CONSOI CONSOA CONSONA et ancienne enquête DREAL","à partir de production_totale et consotot (le min)","à partir des consommations, productions et puissances par énergie","à partir de la conso et de la prodction de chaque type d’énergie (le max)","pac + autres enr","dechets_internes + autre_chaleur_recuperee","fioul lourd + fioul domestique","autres + gpl + chaud electriques"]],"container":"<table class=\"compact\">\n  <thead>\n    <tr>\n      <th>Champs_ajoutes_calcules<\/th>\n      <th>definition<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"pageLength":21,"search":false,"order":[],"autoWidth":false,"orderClasses":false,"lengthMenu":[10,21,25,50,100]}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->


**Nota : Détermination de l'ensemble des champs d'une fiche réseau de chaleur de l'enquête à partir des champs MECC : **    

<!--html_preserve--><div id="htmlwidget-f85726349c57939f39b7" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-f85726349c57939f39b7">{"x":{"filter":"none","fillContainer":false,"autoHideNavigation":true,"data":[["Nom du réseau de chaleur :","Ce réseau de chaleur :","Date de mise en service :","Date de mise à l’arrêt :","Quelle est l'implantation géographique du réseau de chaleur ?","Si vous le souhaitez, vous pouvez nous adresser un fichier image ou pdf pour que nous cartographions plus précisément les bâtiments desservis par le réseau.","filecount - Si vous le souhaitez, vous pouvez nous adresser un fichier image ou pdf pour que nous cartographions plus précisément les bâtiments desservis par le réseau.","Vente de chaleur :","[Chaleur vendue ou livrée en 2018 (MWh)] [] Ampleur des raccordements :","[Nombre de points de livraison] [] Ampleur des raccordements :","[Nombre d'équivalents-logements raccordés] [] Ampleur des raccordements :","Longueur du réseau :","Présence d'une unité de cogénération :","[résidentiels] Type de bâtiments raccordés :","[tertiaires] Type de bâtiments raccordés :","[équipements publics] Type de bâtiments raccordés :","[locaux industriels] Type de bâtiments raccordés :","[Autre] Type de bâtiments raccordés :","Fait-il l’objet d’un classement ?","Gestion du réseau :","[Autre] Gestion du réseau :","Nom du gestionnaire du réseau :","[Id du réseau de chaleur :]","[Nom RC préexistant : ]","[bois] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[biogaz] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[autre biomasse] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[géothermie] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[solaire thermique] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[autre(s) type(s) d'EnR] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[récupération de chaleur des usines d'incinération des ordures ménagères (UIOM)] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[autre récupération de chaleur industrielle] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[autre(s) forme(s) d'énergie de récupération] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[gaz naturel] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[charbon] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[fioul] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[autre forme d'énergie fossile] Quel(s) type(s) d'énergie alimente(nt) ce réseau ?","[Quantité de bois consommée [en tonnes]] Données chaudière bois énergie","[Puissance thermique installée [en kW]] Données chaudière bois énergie","[Énergie primaire consommée [en MWh]] Données chaudière bois énergie","[Puissance thermique installée [en kW]] Données chaudière biogaz","[Énergie primaire consommée [en MWh]] Données chaudière biogaz","[Puissance thermique installée [en kW]] Données chaudière autre biomasse","[Énergie primaire consommée [en MWh]] Données chaudière autre biomasse","[Puissance thermique installée [en kW]] Données géothermie","[Puissance thermique installée [en kW]] Données solaire thermique","[Puissance thermique installée [en kW]] Données autre(s) type(s) d'énergie renouvelable","[Énergie primaire consommée [en MWh]] Données autre(s) type(s) d'énergie renouvelable","[Puissance thermique installée [en kW]] Données usines d'incinération des ordures ménagères (UIOM)","[Énergie primaire consommée [en MWh]] Données usines d'incinération des ordures ménagères (UIOM)","[Puissance thermique installée [en kW]] Données sur la récupération de chaleur industrielle","[Chaleur récupérée en 2018 [en MWh]] Données sur la récupération de chaleur industrielle","[Puissance thermique installée [en kW]] Données sur les autres types d'énergie de récupération","[Énergie primaire consommée [en MWh]] Données sur les autres types d'énergie de récupération","[Puissance thermique installée [en kW]] Données sur la ou les chaudière(s) gaz naturel","[Énergie primaire consommée [en MWh]] Données sur la ou les chaudière(s) gaz naturel","[Puissance thermique installée [en kW]] Données sur la ou les chaudière(s) charbon","[Énergie primaire consommée [en MWh]] Données sur la ou les chaudière(s) charbon","[Puissance thermique installée [en kW]] Données sur la ou les chaudière(s) fioul","[Énergie primaire consommée [en MWh]] Données sur la ou les chaudière(s) fioul","[Puissance thermique installée [en kW]] Données sur la ou les autres types de chaudière fossile","[Énergie primaire consommée [en MWh]] Données sur la ou les autres types de chaudière fossile","Niveau d’émission en CO2 :",null],["id01","id00","id06","id15","id02","id03","id03_filecount","id07","id11_SQ001_SQ001","id11_SQ002_SQ001","id11_SQ003_SQ001","id09","id08","id10_SQ001","id10_SQ002","id10_SQ003","id10_SQ004","id10_other","id12","id13","id13_other","id04","id14_SQ001","id14_SQ002","tec01_SQ001","tec01_SQ002","tec01_SQ003","tec01_SQ004","tec01_SQ005","tec01_SQ006","tec01_SQ007","tec01_SQ008","tec01_SQ009","tec01_SQ010","tec01_SQ011","tec01_SQ012","tec01_SQ013","tec02_SQ000","tec02_SQ001","tec02_SQ002","tec03_SQ001","tec03_SQ002","tec04_SQ001","tec04_SQ002","tec05_SQ001","tec06_SQ001","tec07_SQ001","tec07_SQ002","tec08_SQ001","tec08_SQ002","tec09_SQ001","tec09_SQ003","tec10_SQ001","tec10_SQ002","tec11_SQ001","tec11_SQ002","tec12_SQ001","tec12_SQ002","tec13_SQ001","tec13_SQ002","tec14_SQ001","tec14_SQ002","tec16",null],["nom_du_reseau","en_projet_ou_en_service_statut et date_de_mise_en_service_du_reseau, en fonctionnement par défaut","date_de_mise_en_service_du_reseau","-","geom sigloire ou centroîde depcom","-","-","booleen_vente","min(production_totale, consotot)","nb_de_points_de_livraison","nbre_d_equivalents_logements_raccordes","longueur_reseau_km_*1000","cogeneration","type_de_batiments_raccordes = resid-mixte ou CONSOR &gt; 0","type_de_batiments_raccordes = mixte-ter ou CONSOT &gt; 0","-","type_de_batiments_raccordes = ind ou CONSOI &gt; 0","-","booleen_classe_zp","regie_affermage_concession_industriel _bailleur_universite_hopital_mode_de_gestion","-","gestionnaire","cle_sigloire","nom_du_reseau","max(puissance_biomasse_solide, conso_biomasse_solide, production_biomasse_solide) &gt; 0 ou type_nrj sigloire == biomasse","max(puissance_biogaz, conso_biogaz, production_biogaz) ou type_nrj sigloire == biomasse","max(puissance_biomasse_solide, conso_biomasse_solide, production_biomasse_solide) &gt; 0 ou type_nrj sigloire == biomasse","max(puissance_geothermie, production_geothermie) &gt; 0","max(puissance_solaire_thermique, production_solaire_thermique) &gt; 0","max(puissance_pac, puissance_autres_enr, production_autres_enr) &gt; 0","max(puissance_uiom, production_uiom, conso_uiom) &gt; 0 ou type_nrj sigloire == uiom","max(puissance_chaleur_industriel, production_chaleur_industriel) &gt; 0","max(puissance_autre_chaleur_recuperee, puissance_dechets_internes, production_autre_chaleur_recuperee, production_dechets_internes) &gt;0","max(puissance_gaz_naturel, conso_gaz_naturel, production_gaz_naturel) &gt; 0 ou ou type_nrj sigloire == gaz","max(puissance_charbon, production_charbon) &gt; 0","max(puissance_fioul_domestique, puissance_fioul_lourd, conso_fod, production_fioul_domestique,  production_fioul_lourd) &gt; 0 ou type_nrj sigloire == fioul","max(puissance_gpl, puissance_chaudieres_electriques, puissance_autres, production_gpl, production_chaudieres_electriques, production_autres) &gt; 0","-","puissance_biomasse_solide","max(conso_biomasse_solide, production_biomasse_solide)","puissance_biogaz","max(conso_biogaz, production_biogaz)","puissance_biomasse_solide","max(conso_biomasse_solide, production_biomasse_solide)","puissance_geothermie","puissance_solaire_thermique","puissance_pac + puissance_autres_enr","production_autres_enr","puissance_uiom","max(production_uiom, conso_uiom)","puissance_chaleur_industriel","production_chaleur_industriel","puissance_autre_chaleur_recuperee + puissance_dechets_internes","production_autre_chaleur_recuperee + production_dechets_internes","puissance_gaz_naturel","max(conso_gaz_naturel, production_gaz_naturel)","puissance_charbon","production_charbon","puissance_fioul_domestique + puissance_fioul_lourd","max(conso_fod, production_fioul_domestique) + production_fioul_lourd","puissance_gpl + puissance_chaudieres_electriques + puissance_autres","production_gpl + production_chaudieres_electriques + production_autres","contenu_co2_kg_kwh_","code_commune"]],"container":"<table class=\"compact\">\n  <thead>\n    <tr>\n      <th>lib_quest<\/th>\n      <th>codes_quest<\/th>\n      <th>codes_MECC<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"pageLength":64,"search":false,"order":[],"autoWidth":false,"orderClasses":false,"lengthMenu":[10,25,50,64,100]}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

A partir de ces informations, on met au format limesurvey les données compilées par la MECC.  
Voici les différentes étapes du traitement :   

* charger le tableur calc de la MECC,   
* convertir les types des différents champs,   
* redresser certaines données (par exemple : saisie d'un _x_ pour indiquer un type d'énergie, dans une colonne censée indiquer des valeurs de productions, ou mettre les _non renseigné_ à NA),  
* charger les données sigloire, principalement pour récupérer l'implantation géographique des réseaux, convertir les types et redresser certaines valeurs,  
* joindre les données sigloire aux données MECC,  
* renommer les champs MECC ou réaliser des calculs à partir de ces derniers pour coller à la structure type d'une fiche réseau de chaleur limesurvey, conformément à ce qui est décrit dans le tableau ci-dessus,    
* mettre à jour le code commune, le code epci,
* sélectionner les seuls champs utiles.



```
## [1] TRUE
```

On exporte une première table, comportant une ligne par réseau de chaleur, rassemblant toutes les informations sur ces derniers, exceptées celles liées aux répondants (nom, prénom, email...). Cette table, exportée sous _LIVRABLES/table_RC_type_LS.csv_, présente des intitulés de colonnes adaptés à limesurvey qui sont donc peu compréhensibles par un être humain... La table des attributs, exportée précédemment sous _LIVRABLES/table_attribut_enquete_RC.csv_ permet de comprendre les intitulés. Egalement les tableaux de correspondance des champs reproduits plus haut permettent de retrouver le sens des codes de questions. 

Cette table des réseaux n'est qu'une première adaptation des données fournies par la MECC, **c'est selon cette structure qu'il conviendra à l'avenir de fournir les données à SCTE** (pour sigloire, pour éventuelle modifications des réponses préremplies...).    
Elle est encore loin de la structure de la table des réponses. Pour importer des réponses pré-remplies dans limesurvey, il est nécessaire de disposer d'un tableau qui ne comporte qu'une ligne par répondant. Lorsqu'un participant est concerné par plusieurs réseaux, on doit mettre bout à bout la soixantaine d'informations de chaque réseau de chaleur :  
![](img/format_large.png)
La seconde étape des traitements consiste donc à :  

* mettre au format extra-large le tableau des réseaux de chaleur, 
* à l'enrichir de quelques d'attributs (nb de réseaux par commune, par EPCI),  
* et à l'associer à la table des répondants (une ligne par commune et une ligne par EPCI).  



On vérifie au passage qu'on a prévu suffisamement de fiches réseaux par répondant : au maximum, un EPCI posséde 13 réseaux de chaleur et le questionnaire comporte 20 fiches réseaux par questionnaire.  




La table des réponses pré-remplie, prête à être importée dans limesurvey, est exportée sous LIVRABLES/2019-11-13_1363_rep_a_importer.csv. Le chiffre 1363 correspond au nombre de réponses à importer.  

### Paramètres d'enregistrement csv d'une *table de réponses* à réimporter dans LS
En cas de modification manuelle de la base des réponses à partir de calc, les paramètres d'exports sont :   
* séparateur : tabulation,   
* encodage : UTF-8,   
* séparateur de décimale : .   

![](img/Enregis_csv_rep_depuis_calc.png)



## Finaliser la base de sondage en rattachant chaque organisme enquêté à son ou ses réseau(x) de chaleur  
Pour finir, on complète la base de sondage avec les attributs de noms et d'identifiants de réseaux de chaleur présents dans la table des réponses.  


La table des participants, prête à être importée dans limesurvey, est exportée sous  *LIVRABLES/2019-11-13_1363_participants_a_importer.csv* pour la table complète et *LIVRABLES/2019-11-13_72_participants_epci_a_importer.csv* pour la table d'initialisation du questionnaire. 

## Base de publipostage
Un envoi papier doublonne l'envoi de courriel.
Une base de publipostage rassemblant les adresses postales et les données de la base des participants des 71 epci de la région + l'Ile d'Yeu est constituée.



Elle est enregistrée sous _LIVRABLES/publipostage.csv_.

## Réaliser des tests

Il faut d'abord des testeurs avec des attributs qui vont bien.   


Le fichier des testeurs s'appelle _8_testeurs.csv_, Ce fichier est enregistré dans le repertoire _tests_ des livrables du projet et est prêt pour l'import limesurvey.    


Il leur faut aussi des réponses pré-remplies fictives, piochées dans la table des réponses.   


Le fichier des reponses des testeurs s'appelle 2019-11-13_7_rep_test_a_importer.csv. Le chiffre 7 correspond au nombre de réponses à importer. Ce fichier est enregistré dans le repertoire _tests_ des livrables du projet et est prêt pour l'import limesurvey.

## L'exploitation de l'enquête

### passage du format extralarge au format large





### Récupérer les identifiants cog à partir de la localisation osm

### Dédoublonnage
Plusieurs réponses peuvent porter sur un même réseau de chaleur.
Pour les réseaux pré-identifiés décrits à partir des réponses pré-saisies, on a gardé trace de l'identifiant du réseaux dans les champs de types rcX_id14_SQ001 et de leur nom initial dans rcXid14_SQ002.
Pour les nouveaux réseaux, créer un identifiant à partir du code commune, en suivant la numérotation (par code_commune) précédente. On peut créer un indice d'identification des doublons en regardant leur nombre par commune, leur proximité géographique.

Régles d'élimination des doublons :   

- on conserve (champ par champ ?) ce qui a bougé par rapport à la réponse présaisie,   
- sur un RC donné en doublon éliminer les réponses qui correspondent à "ce réseau est inconnu de mes services". 
- si ça ne suffit pas, on regarde si très différents ?   
- on appelle la MECC ?   

## SGBD

### MCD
Il comprendra : 

- une table des personnes interrogées (par millésime ? ou avec attributs millesimés ?),
- une table des réseaux comportant les données présaisies par millésime (df RC_1, exportés sous _LIVRABLES/table_RC_type_LS.csv_),
- une table des réponses présaisies par millésime (format extra-large, ie avec une ligne par répondant),
- une table des attributs (millésimée si changement ?)
- une table des réponses telle qu'exportées de Limesurvey à la clôture de l'enquête, pour chaque millésime,
- une table de synthèse dédoublonnée, redressée de la non réponse, comprenant les infos des différents millésimes (pour la production, la chaleur vendue...).


### Finalisation de la table d'attributs
Etant donnée l'existence de champs communs, on crée une table d'attributs commune pour les participants et les réponses.   

Cette table d'attributs est exportée dans les livrables du projet sous le nom _table_attribut_enquete_RC.csv_.  



### Chargement des tables livrées

```
## [1] TRUE
```

```
## [1] TRUE
```

```
## data frame with 0 columns and 0 rows
```

```
## named list()
```

```
## [1] TRUE
```

```
## data frame with 0 columns and 0 rows
```

```
## named list()
```

```
## [1] TRUE
```

```
## data frame with 0 columns and 0 rows
```

```
## named list()
```

```
## [1] TRUE
```

```
## data frame with 0 columns and 0 rows
```

```
## [1] TRUE
```

Les tables des réseaux de chaleurs, des participants et des réponses présaisies sont chargées dans le SGBD au niveau du schéma `production.reseaux_chaleur`.  
Elles sont commentées au niveau de la table et des champs, même si la table d'attributs est également chargée. Les 4 tables sont millésimées 2019.




<!-- ####### Archives -->
<!-- ## Listes de choix des communes (abondonné, sera récupéré à partir des coordonnées OSM) -->
<!-- Cette liste est crée à partir de cogifier et est exportée en csv. -->



<!-- Les communes de la région sont trop nombreuses pour être listée par limesurvey, il faut créer une liste par par EPCI ou département. -->
